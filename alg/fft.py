import numpy as np


def fft(x):
    n = len(x)
    while n != 1:
        new_n = (n // 2)
        if new_n * 2 != n:
            raise ValueError("len has to be power of 2")
        n = new_n
    return fft0(len(x), x)


def fft0(n, x):
    if n <= 1:
        return x
    else:
        n = n // 2
        x1 = np.zeros(n, dtype=np.complex128)
        x2 = np.zeros(n, dtype=np.complex128)
        for i in range(n):
            x1[i] = x[2 * i]
            x2[i] = x[2 * i + 1]
        g = fft0(n, x1)
        u = fft0(n, x2)
        c = np.zeros(n * 2, dtype=np.complex128)
        for k in range(n):
            root = np.exp(-2j * np.pi * k / (2 * n))
            c[k] = g[k] + u[k] * root
            c[k + n] = g[k] - u[k] * root
    return c


def ifft(x):
    n = len(x)
    while n != 1:
        new_n = (n // 2)
        if new_n * 2 != n:
            raise ValueError("len has to be power of 2")
        n = new_n
    return ifft0(len(x), x) / len(x)


def ifft0(n, x):
    if n <= 1:
        return x
    else:
        n = n // 2
        x1 = np.zeros(n, dtype=np.complex128)
        x2 = np.zeros(n, dtype=np.complex128)
        for i in range(n):
            x1[i] = x[2 * i]
            x2[i] = x[2 * i + 1]
        g = ifft0(n, x1)
        u = ifft0(n, x2)
        c = np.zeros(n * 2, dtype=np.complex128)
        for k in range(n):
            root = np.exp(2j * np.pi * k / (2 * n))
            c[k] = g[k] + u[k] * root
            c[k + n] = g[k] - u[k] * root
    return c
