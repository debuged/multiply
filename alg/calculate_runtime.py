import random
import time

import multiply


def measure_time(src, function, number=100):
    start = time.time_ns() // 1_000_000
    for i in range(number):
        n1 = random.randrange(0, len(src))
        n2 = random.randrange(0, len(src))
        function(src[n1], src[n2])
    end = time.time_ns() // 1_000_000
    return end - start


class Calculate:

    def __init__(self, name, function, data=None):
        self.start_bits = 10000
        self.end_bits = 200000 + self.start_bits
        self.steps_sz = 10000
        self.initial_count = 50
        self.function = function

        self.data = data
        self.name = name
        self.results = []

    def populate_data(self):
        data = self.data
        if data is None:
            data = []

            for n in range(self.start_bits, self.end_bits, self.steps_sz):
                src = [random.randrange(2 ** (n-5), 2 ** (n+5)) for _ in range(100)]
                data.append(src)
        self.data = data

    def run_test(self):
        initial_count = self.initial_count
        self.results = []
        idx = 0
        for n in range(self.start_bits, self.end_bits, self.steps_sz):
            src = self.data[idx]
            idx = idx + 1
            tu = measure_time(src, function=self.function, number=initial_count)
            log_val = (n, tu, initial_count)
            self.results.append(log_val)
            print(log_val)
            if tu > 510:
                initial_count_a = (initial_count / 510 * 500)
                initial_count_b = (initial_count / tu * 500)
                initial_count = round((initial_count_a + initial_count_b) / 2)
                initial_count = max(initial_count, 3)

    def store_test(self, name):
        with open(name, "w") as fp:
            for _, entry in enumerate(self.results):
                entries = [str(e) for _, e in enumerate(entry)]
                fp.write(",".join(entries))
                fp.write("\n")


if __name__ == '__main__':
    c1 = Calculate("normal", int.__mul__)
    print("populating")
    c1.populate_data()
    print("start test __mul__")
    c1.run_test()

    c1.store_test("../data/mul_normal.txt")

    c2 = Calculate("fft", multiply.multiply)
    c2.data = c1.data
    print("start test fft")
    c2.run_test()
    c2.store_test("../data/mul_fft.txt")

