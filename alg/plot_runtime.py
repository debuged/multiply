import matplotlib.pyplot as plt


def load_data(name):
    xdata = []
    ydata = []
    z1data = []
    z2data = []
    with open(name, "r") as fp:
        for line in fp:
            sl = line.rstrip().split(",")
            xdata.append(int(sl[0]))
            ydata.append(int(sl[1]) / int(sl[2]))
            z1data.append(int(sl[1]))
            z2data.append(int(sl[2]))
            # ydata.append(int(sl[2])/int(sl[1]))
    return xdata, ydata, z1data, z2data


def plot_test():
    figure, axis = plt.subplots(2, 1)

    axis[0].set_title("Function")
    axis[1].set_title("Runs")

    xdata, ydata, z1data, z2data = load_data("../data/mul_fft.txt")
    axis[0].plot(xdata, ydata)
    axis[1].plot(xdata, z2data)

    xdata, ydata, z1data, z2data = load_data("../data/mul_normal.txt")
    axis[0].plot(xdata, ydata)
    axis[1].plot(xdata, z2data)
    plt.tight_layout()
    plt.savefig('../data/comparison.png')
    plt.show()


if __name__ == '__main__':
    plot_test()
