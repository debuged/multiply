def discrete_FFT_ToString(n, x, lookup_table, depth=0, useShifted=True):
    if n <= 1:
        return [f"{x[0]}"]
    else:
        n = n // 2
        x1 = [""] * n
        x2 = [""] * n
        for i in range(n):
            x1[i] = f"{x[2 * i]}"
            x2[i] = f"{x[2 * i + 1]}"

        g = discrete_FFT_ToString(n, x1, lookup_table, depth + 1, useShifted)
        u = discrete_FFT_ToString(n, x2, lookup_table, depth + 1, useShifted)

        c = [""] * (2 * n)
        for k in range(n):
            root = lookup_table[k * 2 ** depth]
            # root2 = lookup_table[(len(lookup_table) - k) % len(lookup_table)]
            root2 = lookup_table[(k * 2 ** depth + len(lookup_table) // 2) % len(lookup_table)]
            c[k] = f"(({g[k]}) + (({u[k]}) * {root}))"
            if useShifted:
                c[k + n] = f"(({g[k]}) + (({u[k]}) * {root2}))"
            else:
                c[k + n] = f"(({g[k]}) - (({u[k]}) * {root}))"
    return c


def print_dft(n=2, data=None, reduceExponent=False):
    """
    print the dft formula
    :param data:
    :param reduceExponent:
    :param n:
    :return: None
    """
    length = 2 ** n
    if data is None:
        data = [f"X^({i + 1})" for i in range(length)]
    for ki in range(length):
        print(f"y[{ki + 1}]:", end='')
        for ni in range(length):
            exponent = ki * ni
            if reduceExponent:
                exponent = exponent % length
            print(f"+B^{exponent}*{data[ni]}", end='')
        print(";")


def print_fft(n=2, data=None, useShifted=False):
    length = 2 ** n
    if data is None:
        data = [f"X^({i + 1})" for i in range(length)]
    roots = [f"B^{k}" for k in range(length)]
    o = discrete_FFT_ToString(len(data), data, roots, useShifted=useShifted)
    for k, line in enumerate(o):
        print(f"y1[{k}]:ratsimp({line});")


def print_inv_dft(n=2):
    """
    print the inverse dft formula
    :param n:
    :return: None
    """
    length = 2 ** n
    for ki in range(length):
        print(f"x[{ki + 1}]:(", end='')
        for ni in range(length):
            print(f"+I^{ki * ni}*y[{ni + 1}]", end='')
        print(f")*L;")


if __name__ == '__main__':
    p = 3
    data_in = [0] * 2 ** p
    TICK = 2
    data_in[TICK] = 1

    print_dft(n=p, data=data_in, reduceExponent=True)
    print('"---;";')
    print_fft(n=p, data=data_in, useShifted=False)
