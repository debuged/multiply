import math

import numpy as np

from multiply.__multiply_impl import fill_number_into_list
from multiply.fft.fft import get_fft_coefficients


def dft(x):
    """
    discrete Fourier Transform with complex numbers
    Source https://jakevdp.github.io/blog/2013/08/28/understanding-the-fft/
    Compute the discrete Fourier Transform of the 1D array x
    """
    x = np.asarray(x, dtype=float)
    N = x.shape[0]
    n = np.arange(N)
    k = n.reshape((N, 1))
    M = np.exp(-2j * np.pi * k * n / N)
    return np.dot(M, x)


def idft(x):
    """
    inverse discrete Fourier Transform with complex numbers
    :param x:
    :return:
    """
    x = np.asarray(x, dtype=np.complex128)
    N = x.shape[0]
    n = np.arange(N)
    k = n.reshape((N, 1))
    M = np.exp(2j * np.pi * k * n / N)
    return np.dot(M, x) / len(x)


def ddft(x, w=None, mod=None, useRootTable=True):
    """
    Slow Fourier Transformation for integers based on a residue field
    :param x: input list
    :param w: overwrite w
    :param mod: overwrite modulus, (should/must) be a prime number
    :param useRootTable: Use RootLookupTable
    :return: transformed list,
        the length will be a power of 2,
        if the input length was not a power of 2 the next bigger length is used instead,
        the missing input digits are integrated as Zero.
    """
    if len(x) < 2:
        return [x[k] for k in range(len(x))]

    n = int(math.log2(len(x)))
    if 2 ** n < len(x):
        n = n + 1

    # x[i] in [0..2**n)
    le = 2 ** n
    y = [0] * le

    if w is None or mod is None:
        w, mod = get_fft_coefficients(le)

    lookup_table = [pow(w, i, mod) for i in range(le)]
    for ki in range(len(y)):
        sigma = 0
        for ni in range(len(x)):
            '''
            reduce 
            4**(z+2**n)
            4**(z+2**n) = 4**z * 2**(2**n) * 2**(2**n) = 4**z
            4**(z+2**n) ≡ 4**z mod 2**(2**n) + 1
            '''
            if useRootTable:
                root = lookup_table[(ki * ni) % le]
            else:
                root = pow(w, ki * ni, mod)
            sigma = (sigma + x[ni] * root) % mod
        y[ki] = sigma
    return y


def iddft(x, w=None, mod=None, useRootTable=True):
    """
    Slow Inverse Fourier Transformation for integers based on a residue field
    :param x: input list
    :param w
    :param mod: overwrite modulus, (should/must) be a prime number
    :param useRootTable: Use RootLookupTable
    :return: back transformed list,
        the length will be a power of 2,
        if the input length was not a power of 2 the next bigger length is used instead,
        the missing input digits are integrated as Zero.
    """
    if len(x) < 2:
        return [x[k] for k in range(len(x))]

    n = int(math.log2(len(x)))
    if 2 ** n < len(x):
        n = n + 1

    # x[i] in [0..2**n)
    le = 2 ** n
    y = [0] * le

    if w is None or mod is None:
        w, mod = get_fft_coefficients(le)

    lookup_table = [pow(w, i, mod) for i in range(le)]
    for ki in range(len(y)):
        sigma = 0
        for ni in range(len(x)):
            if useRootTable:
                root = lookup_table[(le * le - (ki * ni)) % le]
            else:
                root = pow(w, -ki * ni, mod)
            sigma = (sigma + x[ni] * root) % mod
        y[ki] = (sigma * pow(len(y), -1, mod)) % mod
    return y


def test_permutations(n=1, show=False):
    # test all permutations
    mod = 2 ** (2 ** n) + 1
    x = [0] * (2 ** n)
    for num in range(mod ** len(x)):
        fill_number_into_list(num, x, mod)
        y = ddft(x, n)
        x2 = iddft(y, n)
        if not np.allclose(x, x2):
            if show:
                print("Error ", x, y, x2)
            return False
        if not np.allclose(x, [0] * (2 ** n)) and np.allclose(x, y):
            if show:
                print("Error ", x, y, x2)
            return False
    return True
