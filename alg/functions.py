def Fn(n):
    if n < 0:
        raise ValueError("non-negative integer expected")
    return 2 ** (2 ** n) + 1
