def get_fft_coefficients(length):
    """
    :param length:
    :return: w, modulus modulus>=2**32
    """
    '''
    n = 5
    c = 0
    d = 2 * c + 1
    w = 2 ** d
    mod = 2 ** (d * 2 ** n) + 1
    # https://jakevdp.github.io/blog/2013/08/28/understanding-the-fft/
    # https://de.wikipedia.org/wiki/Sch%C3%B6nhage-Strassen-Algorithmus
    '''
    if length == 1:
        # return a dummy value
        return 2 ** 0, 2 ** 0 + 1
    if length < 2:
        raise ValueError(f"length ({length}) is smaller then 2")
    mod_exponent = length // 2
    w_exponent = 1

    if mod_exponent < 32:
        w_exponent = 32 // mod_exponent
        mod_exponent = mod_exponent * w_exponent
    return 2 ** w_exponent, 2 ** mod_exponent + 1


def check_fft_coefficients(length, w, mod, checkCoefficients):
    """

    :param length:
    :param w:
    :param mod:
    :param checkCoefficients:
    :return: w, mod, lookup_table
    """
    n = length
    while n != 1:
        new_n = (n // 2)
        if new_n * 2 != n:
            raise ValueError("len has to be power of 2")
        n = new_n
    if mod is None or w is None:
        w, mod = get_fft_coefficients(length)
    lookup_table = [pow(w, i, mod) for i in range(length)]
    if checkCoefficients:
        if lookup_table[-1] * w % mod != 1:
            raise ValueError(f"bad w/w != 1 1/w={lookup_table[-1]} w={w} mod={mod} length={length}")
        if lookup_table[len(lookup_table) // 2] + 1 != mod:
            raise ValueError(f"bad -1+1 != 0 -1={lookup_table[len(lookup_table) // 2]} w={w} mod={mod} length={length}")
    return w, mod, lookup_table


def fft(x, w=None, mod=None, useShifted=True, checkCoefficients=True):
    if len(x) < 2:
        return [x[i] for i in range(len(x))]
    w, mod, lookup_table = check_fft_coefficients(length=len(x), w=w, mod=mod, checkCoefficients=checkCoefficients)
    r = __fft(len(x), x, lookup_table, mod, depth=0, useShifted=useShifted)
    return r


def __fft(n, x, lookup_table, mod, depth=0, useShifted=True):
    if n <= 1:
        return x
    else:
        n = n // 2
        x1 = [0] * n
        x2 = [0] * n
        for i in range(n):
            x1[i] = x[2 * i]
            x2[i] = x[2 * i + 1]
        g = __fft(n, x1, lookup_table, mod, depth + 1, useShifted=useShifted)
        u = __fft(n, x2, lookup_table, mod, depth + 1, useShifted=useShifted)
        c = [0] * (2 * n)
        for k in range(n):
            root = lookup_table[k * (2 ** depth)]
            # root2 = lookup_table[(len(lookup_table) - k) % len(lookup_table)]
            root2 = lookup_table[(k * 2 ** depth + len(lookup_table) // 2) % len(lookup_table)]

            c[k] = (g[k] + u[k] * root) % mod
            if useShifted:
                c[k + n] = (g[k] + u[k] * root2) % mod
            else:
                c[k + n] = (g[k] - u[k] * root) % mod
    return c


def ifft(x, w=None, mod=None, useShifted=False, checkCoefficients=True):
    if len(x) < 2:
        return [x[i] for i in range(len(x))]
    w, mod, lookup_table = check_fft_coefficients(length=len(x), w=w, mod=mod, checkCoefficients=checkCoefficients)
    r = __ifft(len(x), x, lookup_table, mod, useShifted=useShifted)
    for i in range(len(r)):
        r[i] = (r[i] * pow(len(r), -1, mod)) % mod
    return r


def __ifft(n, x, lookup_table, mod, depth=0, useShifted=True):
    if n <= 1:
        return x
    else:
        n = n // 2
        x1 = [0] * n
        x2 = [0] * n
        for i in range(n):
            x1[i] = x[2 * i]
            x2[i] = x[2 * i + 1]
        g = __ifft(n, x1, lookup_table, mod, depth + 1, useShifted=useShifted)
        u = __ifft(n, x2, lookup_table, mod, depth + 1, useShifted=useShifted)
        c = [0] * (2 * n)
        for k in range(n):
            root = lookup_table[(len(lookup_table) - k * 2 ** depth) % len(lookup_table)]
            root2 = lookup_table[(len(lookup_table) // 2 + len(lookup_table) - k * 2 ** depth) % len(lookup_table)]
            c[k] = (g[k] + u[k] * root) % mod
            if useShifted:
                c[k + n] = (g[k] + u[k] * root2) % mod
            else:
                c[k + n] = (g[k] - u[k] * root) % mod
    return c
