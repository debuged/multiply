import math
import random

from multiply.fft.fft import get_fft_coefficients, fft, ifft, check_fft_coefficients


def multiply_int(a, b, debug=False):
    # use better coefficients
    base = 2**29
    x1 = number_to_list(a, base)
    x2 = number_to_list(b, base)
    length0 = max(len(x1), len(x2))
    length = 2
    while length < length0:
        length = length * 2
    length = length * 2
    y1 = [0] * length
    y2 = [0] * length
    for idx, value in enumerate(x1):
        y1[idx] = value
    for idx, value in enumerate(x2):
        y2[idx] = value

    w, mod = get_fft_coefficients(length)
    if mod < 2 ** 32:
        raise ValueError("bad base")
    d = round(math.log2(w))
    if debug:
        print("d mod", d, mod)
    if d % 2 == 1000:
        h = round(math.log2(mod - 1))
        _mod = 2 ** ((h // d) * (d + 1)) + 1
        _w = 2 ** (d + 1)
        mod = _mod
        w = _w

    z1 = fft(y1, w=w, mod=mod)
    z2 = fft(y2, w=w, mod=mod)
    if debug:
        print("z1", z1)
        print("z2", z2)

    z = [(z1[idx] * z2[idx]) % mod for idx in range(len(z1))]
    if debug:
        print(z)
    z = [(z1[idx] * z2[idx]) % 2 ** 32 for idx in range(len(z1))]
    if debug:
        print(z)

    z = [(z1[idx] * z2[idx]) for idx in range(len(z1))]
    if debug:
        print(z)

    y = ifft(z, w=w, mod=mod)
    if debug:
        print("mod", mod)
        print("y", y)
    c = 0
    for idx, value in enumerate(y):
        c = c + value * base ** idx
    if debug:
        print(c)
        print(a * b)
    return c


def multiply_list(a, b, mod):
    # mod = 2** (d * length/2) + 1
    return [0, 0]


def number_to_list(value, base):
    if base < 2:
        raise ValueError("base has to be bigger then one")
    result = []
    while value != 0:
        result.append(value % base)
        value = value // base
    return result


def fill_number_into_list(num, destination_list, base):
    """
    Converts the input number into a number of base "base".
    Writes the digits into the destination_list, staring with LSB at index Zero
    num = x[0] + x[1]*base + x[2]*base**2 + ... + remainder*base**len(x)
    :param destination_list: Writes the digits into the destination_list, staring with LSB at index Zero
    :param num: Input number
    :param base: new Base, the base must not be Zero
    :return: the remainder, the value will be non-zero if it didn't fit into the destination_list
    """
    if base == 0:
        raise ValueError("base is zero")
    for idx in range(len(destination_list)):
        destination_list[idx] = num % base
        num = num // base
    return num


if __name__ == '__main__':

    m = 1
    for i in range(20):
        m_new = multiply_int(m, 5, debug=True)
        print(m * 5)
        if m * 5 != m_new:
            print("error")
            print(m * 5)
            print(m_new)
            break
        m = m_new
