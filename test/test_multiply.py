import random
import unittest

from multiply.__multiply_impl import *
import multiply as ml


class MultiplyTestCase(unittest.TestCase):

    def test_multiply_zero(self):
        c = ml.multiply(0, 0)
        self.assertEqual(c, 0)

    '''    
    def test_multiply_one(self):
        c = multiply.multiply(0, 0)
        self.assertEqual(c, 0)
    '''

    def test_multiply_sequence(self, number_of_tests=1000, base=5):
        value = 1
        for c in range(number_of_tests):
            new_value = multiply_int(value, base)
            self.assertEqual(base*value, new_value, msg=f"b={base} c={c} base**c*base!=base**(c+1)")
            value = new_value

    def test_multiply_random_int(self, limit=1000, number_of_tests=10):
        for _ in range(number_of_tests):
            a = random.randrange(0, 2 ** limit)
            b = random.randrange(0, 2 ** limit)
            c1 = a * b
            c2 = multiply_int(a, b)
            self.assertEqual(c1, c2)

    def test_multiply_zero_list(self, limit=10):
        c0 = [0, 0]
        c2 = multiply_list([0], [0], mod=2)
        self.assertEqual(c0, c2)

        c2 = multiply_list([1], [0], mod=2)
        self.assertEqual(c0, c2)

        c2 = multiply_list([0], [1], mod=2)
        self.assertEqual(c0, c2)

    def test_base_transform(self, limit=10):
        expected1 = []
        result1 = number_to_list(0, base=2)

        self.assertEqual(expected1, result1)

        expected2 = [1]
        result2 = number_to_list(1, base=2)
        self.assertEqual(expected2, result2)

    def test_base_transform_bad_modulus(self):
        self.assertRaises(ValueError, number_to_list, 0, 0)

    def test_base_transform_bad_modulus(self):
        self.assertRaises(ValueError, number_to_list, 0, 0)


if __name__ == '__main__':
    unittest.main()
