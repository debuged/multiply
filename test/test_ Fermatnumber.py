import unittest

from alg import functions


def make_FermatNumber_test_case(function):
    class BasicsTestCase(unittest.TestCase):
        def testF0(self):
            self.assertEqual(function(0), 3)

        def testFnNegative1(self):
            self.assertRaises(ValueError, function, -10)

        def testFnValues(self):
            for n, expected in enumerate([3, 5, 17, 257, 65537, 4294967297, 18446744073709551617]):
                self.assertEqual(function(n), expected, msg=f"Fn({n})")

    return BasicsTestCase


class TestSimpleImplementation(make_FermatNumber_test_case(functions.Fn)):
    pass


if __name__ == '__main__':
    unittest.main()
