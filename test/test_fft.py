import random
import unittest

import numpy as np
from numpy import linalg as la

import alg.dft
import alg.fft
import multiply.fft.fft

LONG_TEST = False  # 0 false 1 true


def test_range0(self, function):
    self.assertRaises(ValueError, function, [0] * 3)
    pass


class CheckFFT(unittest.TestCase):
    def test_range(self):
        test_range0(self, alg.fft.fft)
        test_range0(self, alg.fft.ifft)
        test_range0(self, multiply.fft.fft.fft)
        test_range0(self, multiply.fft.fft.ifft)

    def test_coefficients(self):
        length = 1
        for i in range(12 + 2 * LONG_TEST):  # 2**12 .. 2**14
            length = length * 2
            w, mod = multiply.fft.fft.get_fft_coefficients(length)
            lookup_table = [pow(w, i, mod) for i in range(length)]
            one_count = sum([1 if lookup_table[k] == 1 else 0 for k in range(length)])
            self.assertEqual(one_count, 1,
                             msg=f"more then 1, '1' found w={w} mod={mod} length={length}")
            self.assertEqual(lookup_table[-1] * w % mod, 1,
                             msg=f"bad w/w != 1 1/w={lookup_table[-1]} w={w} mod={mod} length={length}")
            self.assertEqual((lookup_table[len(lookup_table) // 2] + 1) % mod, 0,
                             msg=f"bad -1+1 != 0 -1={lookup_table[len(lookup_table) // 2]} "
                                 f"w={w} mod={mod} length={length}")
            self.assertLess(w, mod,
                            msg=f"w>=mod w={w} mod={mod} length={length}")

    def test_simple_transformation(self):
        """
        Simple test x->fft(x)
         1,0,0,0, ... -> b**0,b**0,b**0, ...
         0,1,0,0, ... -> b**0,b**1,b**2, ...
         0,0,1,0, ... -> b**0,b**2,b**4, ...
        :return:
        """
        for i in range(1, 7 + 3 * LONG_TEST + 1):  # 2**1 .. 2**7/2**10
            length = 2 ** i
            w, mod = multiply.fft.fft.get_fft_coefficients(length)
            for tick in range(length):
                x1 = [0] * length
                x1[tick] = 1
                y1 = multiply.fft.fft.fft(x1)
                x2 = [0] * length
                x2[-tick] = length
                y2 = multiply.fft.fft.ifft(x2)

                expected = [pow(w, tick * k, mod) for k in range(length)]

                self.assertEqual(y1, expected)
                self.assertEqual(y2, expected)


def make_complex_fft_forward_backward_test(function, inverse_function, length_power_of_2=16):
    class DFTBijective(unittest.TestCase):
        def test_bijective(self):
            for i in range(20):
                length = 1
                for _ in range(length_power_of_2 + 1):
                    x = np.random.random(length)
                    y = function(x)
                    i_y = inverse_function(y)

                    self.assertTrue(np.allclose(x, i_y),
                                    msg=f"not close norm(x,inverse_y)={la.norm(x - i_y)} length={length}")
                    length = length * 2

    return DFTBijective


def make_discrete_fft_forward_backward_test(function, inverse_function, length_power_of_2=4):
    class DiscreteDFTBijective(unittest.TestCase):
        def test_bijective(self):
            for i in range(20):
                length = 1
                for _ in range(length_power_of_2 + 1):
                    # mod = 2 ** length + 1
                    w, mod = multiply.fft.fft.get_fft_coefficients(length)
                    x = [random.randrange(0, mod) for _ in range(length)]
                    y = function(x)
                    i_y = inverse_function(y)

                    norm = sum([(x[i] - i_y[i]) ** 2 for i in range(length)]) ** (1 / 2)
                    self.assertEqual(x, i_y,
                                     msg=f"not close norm(x,inverse_y)={norm} length={length} "
                                         f"f1={str(function)} f2={str(inverse_function)}")
                    length = length * 2

    return DiscreteDFTBijective


def make_discrete_fft_equal_test(function1, function2, length_power_of_2=4):
    class DiscreteDFTEqual(unittest.TestCase):
        def test_equal(self):
            for i in range(20):
                length = 1
                for _ in range(length_power_of_2 + 1):
                    # mod = 2 ** length + 1
                    w, mod = multiply.fft.fft.get_fft_coefficients(length)
                    x = [random.randrange(0, mod) for i in range(length)]
                    y1 = function1(x)
                    y2 = function2(x)

                    norm = sum([(y1[i] - y2[i]) ** 2 for i in range(length)]) ** (1 / 2)
                    self.assertEqual(y1, y2,
                                     msg=f"not close norm(y1,y2)={norm} length={length} "
                                         f"f1={str(function1)} f2={str(function2)}")
                    length = length * 2

    return DiscreteDFTEqual


class CompareDFT1(make_complex_fft_forward_backward_test(alg.dft.dft, alg.dft.idft,
                                                         length_power_of_2=10 + 2 * LONG_TEST)):
    """
    x = i(f(x))
    this implementation has a Runtime of O(n**2) test till length 2**9
    """
    pass


class CompareFFT2(make_complex_fft_forward_backward_test(alg.fft.fft, alg.fft.ifft,
                                                         length_power_of_2=10 + 2 * LONG_TEST)):
    """
    x = i(f(x))
    test till length 2**12
    """
    pass


class CompareDFTInZ1(make_discrete_fft_forward_backward_test(alg.dft.ddft, alg.dft.iddft,
                                                             length_power_of_2=7 + LONG_TEST * 1)):
    """
    x = i(f(x))
    this implementation has a Runtime of O(n**2) test till length 2**8
    """
    pass


class CompareFFTInZ3(make_discrete_fft_equal_test(alg.dft.ddft, multiply.fft.fft.fft,
                                                  length_power_of_2=7 + LONG_TEST * 1)):
    """
    f1(x) = f2(x)
    """
    pass


class CompareFFTInZ4(make_discrete_fft_equal_test(alg.dft.iddft, multiply.fft.fft.ifft,
                                                  length_power_of_2=7 + LONG_TEST * 1)):
    """
    i1(x) = i2(x)
    """
    pass


if __name__ == '__main__':
    unittest.main()
